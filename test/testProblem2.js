const fs = require("fs");
const problem2 = require("../problem2");
const writefileName = (filename) => {
  fs.appendFile("./data/filenames.txt", filename + "\n", "utf8", (err) => {
    if (err) {
      throw err;
    }
  });
};
problem2(writefileName);
