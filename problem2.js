const fs = require("fs");
const path = require("path");
const problem2 = (writefileName) => {
  let fileName = {
    file1: path.join(__dirname, "./test/data/file1"),
    file2: path.join(__dirname, "./test/data/file2"),
    file3: path.join(__dirname, "./test/data/file3"),
    file4: path.join(__dirname, "./test/data/file4"),
  };
  fs.readFile(
    path.join(__dirname, "./test/data/lipsum.txt"),
    "utf8",
    (err, data) => {
      if (err) {
        throw err;
      } else {
        console.log(data);
        data = data.toUpperCase();
        fs.writeFile(fileName.file1, data, (err) => {
          if (err) {
            throw err;
          } else {
            writefileName(path.basename(fileName.file1));
            fs.readFile(fileName.file1, "utf8", (err, data) => {
              if (err) {
                throw err;
              } else {
                data = data.toLowerCase().split(". ").join("\n ");

                fs.writeFile(fileName.file2, data, (err) => {
                  if (err) {
                    throw err;
                  } else {
                    writefileName(path.basename(fileName.file2));
                    data = data.split("\n").sort().join("\n");
                    fs.writeFile(fileName.file3, data, (err) => {
                      if (err) {
                        throw err;
                      } else {
                        writefileName(path.basename(fileName.file3));
                        fs.readFile(
                          path.join(__dirname, "./test/data/filenames.txt"),
                          "utf8",
                          (err, data) => {
                            if (err) {
                              throw err;
                            } else {
                              data = data.split("\n").forEach((element) => {
                                if (element.length > 0) {
                                  fs.unlink(
                                    path.join(
                                      __dirname,
                                      `./test/data/${element}`
                                    ),
                                    (err) => {
                                      if (err) {
                                        throw err;
                                      }
                                    }
                                  );
                                }
                              });
                            }
                          }
                        );
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    }
  );
};

module.exports = problem2;
