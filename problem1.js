const fs = require("fs");
const path = require("path");
const generateRandomFiles = () => {
  let fileName = [1, 2, 3, 4, 5];
  fs.mkdir("random", (err) => {
    if (err) {
      throw err;
    } else {
      fileName.forEach((element) => {
        fs.writeFile(
          path.join(__dirname, `./test/random/file ${element}`),
          `hello this is file ${element}.json`,
          (err) => {
            if (err) {
              throw err;
            } else {
              console.log(`file ${element} is created`);
            }
          }
        );
      });
      setTimeout(() => {
        fileName.forEach((element) => {
          fs.unlink(
            path.join(__dirname, `./test/random/file ${element}.json`),
            () => {
              if (err) {
                throw err;
              } else {
                console.log(`file ${element} is deleted`);
              }
            }
          );
        });
        fs.rmdirSync("random", { recursive: true });
      }, 3000);
    }
  });
};
module.exports = generateRandomFiles;
